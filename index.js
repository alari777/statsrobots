require('dotenv').config();

const express = require('express');

const app = express();

const server = require('http').Server(app);

const io = require('socket.io')(server);

const Connections = require('./connections');

const connections = new Connections();

const Stats = require('./stats');

// io.set('origins', '18.220.154.71:32772');
// io.set('origins', 'localhost:3000');
// io.set('origins', '3.16.24.219:32773');
server.listen(80);

io.on('connection', () => {
  async function makeStat(table, statNumberClose, statNumberOpen, redisClient, reduceTotal, reduceTotalTotal24) {
    const stat = new Stats(connections, table);
    let resultClose = new Map();
    if (table === 'bsnew_sim_binance_mar_total_sim_test3') {
      resultClose = await stat.closeOrders();
    } else {
      resultClose = await stat.closeOrdersMysql();
    }
    io.emit(statNumberClose, JSON.stringify(Array.from(resultClose.entries())));

    let resultOpen = new Map();
    if (table === 'bsnew_sim_binance_mar_total_sim_test3') {
      resultOpen = await stat.openOrders(redisClient);
    } else {
      resultOpen = await stat.openOrdersMysql(redisClient);
    }
    const resultReduceAll = await stat.reducePercents('total');
    const resultReduceTotal24 = await stat.reducePercents('last24hours');
    const resultReduceAllMysql = await stat.reducePercentsMysql('total');
    const resultReduceTotal24Mysql = await stat.reducePercentsMysql('last24hours');
    const reduceAll = parseFloat(resultReduceAll) + parseFloat(resultReduceAllMysql) + parseFloat(stat.sumPers) + parseFloat(stat.sumPersMysql);
    const reduceAllTotal24 = parseFloat(resultReduceTotal24) + parseFloat(resultReduceTotal24Mysql) + parseFloat(stat.sumPers) + parseFloat(stat.sumPersMysql);
    io.emit(statNumberOpen, JSON.stringify(Array.from(resultOpen.entries())));
    io.emit(reduceTotal, JSON.stringify(reduceAll.toFixed(2)));
    io.emit(reduceTotalTotal24, JSON.stringify(reduceAllTotal24.toFixed(2)));
    // result.forEach((dataset, index) => {
    //   console.log(dataset.symbol);
    // });
  }

  function allStats() {
    setInterval(() => {
      // makeStat('bsnew_sim_binance_mar_total_sim_test1', 'stats35', 'stats351', connections.redisclients.redis1, 'stats352', 'stats353');
      // makeStat('bsnew_sim_binance_mar_total_sim_test2', 'stats36', 'stats361', connections.redisclients.redis1, 'stats362', 'stats363');
      makeStat('bsnew_sim_binance_mar_total_sim_test3', 'stats37', 'stats371', connections.redisclients.redis1, 'stats372', 'stats373');
      // makeStat('bsnew_sim_binance_mar_total_sim_test4', 'stats38', 'stats381', connections.redisclients.redis1, 'stats382', 'stats383');
      makeStat('bsnew_sim_binance_mar_total_sim', 'stats39', 'stats391', connections.redisclients.redis1, 'stats392', 'stats393');
      // makeStat('bsnew_sim_binance_total', 'stats40', 'stats401', connections.redisclients.redis1, 'stats402', 'stats403');
      // makeStat('bsnew_sim_binance_mar_total_sim_test5', 'stats50', 'stats501', connections.redisclients.redis3, 'stats502', 'stats503');
    }, 1 * 1 * 360 * 1000);
  }

  (async function robot() {
    console.log('1.) Starting sctipt ...');
    allStats();

    return true;
  }());
});