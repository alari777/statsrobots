class Binancekeeping {
  constructor(connection, settings) {
    this.redis = connection.redisclients.redismain;
    // this.redispub = connection.redispub;

    this.postgre = connection.postgre;
    this.binance = connection.binance;

    this.depthCacheBinance = settings.depthCacheBinance;
    this.lenBids = settings.lenBids;
    this.lenAsks = settings.lenAsks;

    this.bids = {};
    this.asks = {};

    this.postgreone = connection.connectToPostgre();

    return this;
  }

  getBinanceOrders(markets, ioconnect) {
    let counter = 0;
    this.binance.robot.websockets.depthCache(markets, (symbol, depth) => {
      const bids = this.binance.robot.sortBids(depth.bids);
      const asks = this.binance.robot.sortAsks(depth.asks);

      const lenBids = Object.keys(bids).length;
      const lenAsks = Object.keys(asks).length;

      // let firstBids = this.binance.robot.first(bids);
      // let firstAsks = this.binance.robot.first(asks);

      if (lenBids >= this.lenBids && lenAsks >= this.lenAsks) {
        counter += 1;

        // this.redissubkeep.subscribe('depthcache', () => {
        //   this.redispubkeep.publish('depthcache', symbol);
        // });

        const datasBids = [];
        Object.keys(bids).forEach((key, index) => {
          if (index <= 99) {
            datasBids.push(bids[key]);
          }
        });

        const a = datasBids.reduce((previousValue, currentValue) => {
          return previousValue + currentValue;
        });

        // console.log(symbol, a.toFixed(2));
        ioconnect.emit(`${symbol}-reduce`, a.toFixed(2));

        if (counter === 5000) {
          console.log('Postgre check generall connect. Counter Ticks:', counter);
          counter = 0;
          const sql = 'SELECT * FROM public.monitoringsbinance_mar_total_sim LIMIT 1;';
          // this.postgre.query(sql, () => {});
          this.postgreone.query(sql, (err) => {
            if (err) console.log(err);
          });
        }
      }
    }, this.depthCacheBinance);
  }
}

module.exports = Binancekeeping;