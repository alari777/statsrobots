class Preparation {
  constructor(postgre) {
    this.postgre = postgre;

    this.container = '1';
    this.marketFulls = {};
    this.marketNames = {};

    this.delistMarkets = ['CLOAKBTC', 'MODBTC', 'SALTBTC', 'SUBBTC', 'WINGSBTC'];

    return this;
  }

  async getMarketsForWork() {
    try {
      const marketsObj = await this.getMarketsFromPostgreDB();
      if (marketsObj === null) return null;

      this.marketFulls = marketsObj.f;
      this.marketNames = marketsObj.n;

      return {
        step1: marketsObj,
        step2: 'Redis: innessary',
      };
    } catch (err) {
      return err;
    }
  }

  getMarketsFromPostgreDB() {
    return new Promise((resolve, reject) => {
      let sql = '';

      if (process.env.ROBOT_MODE === 'dev') {
        sql = `SELECT * FROM public.${process.env.MONITORINGSBINANCE_TEST1} WHERE ( container = $1 AND market NOT IN ('BTCUSDT', 'TUSDBTC', 'BCNBTC', 'TRIGBTC', 'ICNBTC', 'CHATBTC', 'BCCBTC') ) ${process.env.POSTGRE_RANGE_COINS};`;
      }

      if (process.env.ROBOT_MODE === 'prod') {
        sql = `SELECT * FROM public.monitoringsbinance_test1 WHERE ( container = $1 AND market NOT IN ('BTCUSDT', 'TUSDBTC', 'BCNBTC', 'TRIGBTC', 'ICNBTC', 'CHATBTC', 'BCCBTC') ) ${process.env.POSTGRE_RANGE_COINS};`;
      }

      if (sql !== '') {
        this.postgre.query(sql, [this.container], (err, result) => {
          if (err) reject(err);

          let counter = 0;
          const marketFulls = [];
          const marketNames = [];
          if (result.rowCount >= 1) {
            result.rows.forEach((dataset) => {
              if (!this.delistMarkets.includes(dataset.market)) {
                marketFulls.push(dataset);
                marketNames.push(dataset.market);
              }
              counter += 1;
              if (result.rowCount === counter) {
                counter = 0;
                const obj = {
                  f: marketFulls,
                  n: marketNames,
                };
                resolve(obj);
              }
            });
          } else {
            resolve(null);
          }
        });
      } else {
        const obj = {
          f: ['no'],
          n: ['no'],
        };
        resolve(null);
      }
    });
  }
}

module.exports = Preparation;