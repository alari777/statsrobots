require('dotenv').config();

const Connections = require('../connections');

const connections = new Connections();

const Stats = require('../stats');

const stat = new Stats(connections, 'bsnew_sim_binance_mar_total_sim_test3');
	
test('to get amount of records less than or equal 25: CLOSE orders', (done) => {
	return stat.closeOrders().then(data => {
		const dataS = data.size;
		expect(dataS).toBeLessThanOrEqual(25);		
		stat.stop( () => {
			done();
		});
		// done(function() {
		// 	console.log("TEST");
		// });
		// done(() => {
		// 	stat.done();
		// });
	});
});
