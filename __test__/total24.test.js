require('dotenv').config();

const Connections = require('../connections');

const connections = new Connections();

const Stats = require('../stats');

const stat = new Stats(connections, 'bsnew_sim_binance_mar_total_sim_test3');
	
test('to calculate percent summa of 24 hours close orders', (done) => {
	return stat.reducePercents('last24hours').then(data => {
		expect(data).toBeDefined();
		// stat.done();
		stat.stop( () => {
			done();
		});
	});
});
