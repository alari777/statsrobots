FROM node:carbon

# Install software 
RUN apt-get install -y git
RUN npm install forever -g
VOLUME /statsrobots

RUN git clone https://alari777@bitbucket.org/alari777/statsrobots.git /statsrobots/

# Create app directory
WORKDIR /statsrobots

EXPOSE 80
CMD git pull origin master && npm install -y && npm start