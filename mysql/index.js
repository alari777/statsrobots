require('dotenv').config();
const mysql = require('mysql');

class Mysql {
    constructor() {
        this.connection = mysql.createConnection({
            host: process.env.MYSQL_HOST,
            user: process.env.MYSQL_USERNAME,
            password: process.env.MYSQL_PASSWORD,
            database: process.env.MYSQL_DB
        });

        return this.connection.connect();
    }

    async doneMysql() {
        return new Promise((resolve, reject) => {
            this.connection.end((err) => {
                if (err) {
                    console.error(err);
                    reject(err);
                }
                resolve(null);
            });
        });
    }
}

module.exports = Mysql;