const Mysql = require('../mysql');

class Stats {
  constructor(connection, table) {
    this.postgre = connection.postgre;
    this.table = table;
    this.sumPers = 0;
    this.sumPersMysql = 0;

    return this;
  }

  reducePercentsMysql(type) {
    let sql = '';

    if (type === 'total') {
      sql = `SELECT ( (ratebuy - ratesell) / ratebuy * (-100) ) as per FROM ${this.table} WHERE (typeordbuy = ? AND typeordsell = ? AND ( DATE_ADD(NOW(), INTERVAL 1 HOUR) < DATE_ADD(timeutcsell, INTERVAL 43802 HOUR)) ) ORDER BY timeutcsell DESC`;
    }

    if (type === 'last24hours') {
      sql = `SELECT ( (ratebuy - ratesell) / ratebuy * (-100) ) as per FROM ${this.table} WHERE (typeordbuy = ? AND typeordsell = ? AND ( DATE_ADD(NOW(), INTERVAL 1 HOUR) < DATE_ADD(timeutcsell, INTERVAL 26 HOUR)) ) ORDER BY timeutcsell DESC`;
    }

    return new Promise((resolve, reject) => {
      const mysql = new Mysql();
      mysql.connection.query({
        sql: sql,
        timeout: 40000,
        values: ['BUY', 'SELL'],
      }, (err, result) => {
        if (err) {
          console.log(err);
          reject(err);
        }

        const persentsD = [];
        if (result.length >= 1) {
          result.forEach((dataset) => {
            const per = parseFloat(dataset.per);
            const perWithComisionTotal = per - (Math.abs(per * 0.15));
            persentsD.push(perWithComisionTotal);
          });

          let resultPers22k = 0;
          if (persentsD.length !== 0) {
            resultPers22k = persentsD.reduce((sum, current) => sum + current, 0);
          } else resultPers22k = 0;

          mysql.connection.end((errm1) => {
            if (errm1) {
              console.error(errm1);
              reject(errm1);
            }
            resolve(resultPers22k);
          });

        } else {
          mysql.connection.end((errm2) => {
            if (errm2) {
              console.error(errm2);
              reject(errm2);
            }
            resolve(0);
          });
        }
      });
    });
  }

  closeOrdersMysql() {
    return new Promise((resolve, reject) => {
      const mysql = new Mysql();
      const sqlLL = `SELECT * FROM ${this.table} WHERE (typeordbuy = ? AND typeordsell = ?) ORDER BY timeutcsell DESC LIMIT 25;`;
      mysql.connection.query({
        sql: sqlLL,
        timeout: 40000,
        values: ['BUY', 'SELL'],
      }, (err, result) => {
        if (err) {
          console.log(err);
          reject(err);
        }

        if (result.length !== 0) {
          result.reverse();
          const map = new Map();
          result.forEach((dataset) => {
            const rbuy = parseFloat(dataset.ratebuy);
            const rsell = parseFloat(dataset.ratesell);
            let per = (rbuy - rsell) / rbuy * 100;
            per *= (-1);
            per = parseFloat(per) - (Math.abs(per * 0.15));

            map.set(dataset.uuidord, {
              symbol: dataset.market,
              persent: per.toFixed(2),
              timeorder: dataset.timeutcsell,
              rate: dataset.ratesell,
            });

            if (result.length === map.size) {
              mysql.connection.end((errm) => {
                if (errm) {
                  console.error(errm);
                  reject(errm);
                }
                resolve(map);
              });
            }
          });
        }
      });
    });
  }

  openOrdersMysql(redisClient) {
    return new Promise((resolve, reject) => {
      const mysql = new Mysql();
      const sqlLL = `SELECT * FROM ${this.table} WHERE (typeordbuy = ? AND typeordsell = ?) ORDER BY timeutcbuy DESC LIMIT 25;`;
      mysql.connection.query({
        sql: sqlLL,
        timeout: 40000,
        values: ['BUY', ''],
      }, (err, result) => {
        if (err) {
          console.log(err);
          reject(err);
        }

        const map = new Map();
        if (result.length !== 0) {
          // result.rows.reverse();
          let sumPersMysql = 0;
          this.sumPersMysql = 0;
          result.forEach( async (dataset) => {
            const buysellnow = await Stats.buyNowValue(redisClient, dataset.market);
            // const part11 = parseFloat(dataset.ratebuy) - parseFloat(buysellnow);
            const rbuy = parseFloat(dataset.ratebuy);
            const rnow = parseFloat(buysellnow);
            let perBuy = (rbuy - rnow) / rbuy * 100;
            perBuy *= -1;
            perBuy = parseFloat(perBuy) - (Math.abs(perBuy * 0.075));
            sumPersMysql += perBuy;

            map.set(dataset.uuidord, {
              symbol: dataset.market,
              persent: perBuy.toFixed(2),
              timeorder: dataset.timeutcbuy,
              rate: dataset.ratebuy,
            });

            if (result.length === map.size) {
              this.sumPersMysql = sumPersMysql;
              mysql.connection.end((errm) => {
                if (errm) {
                  console.error(errm);
                  reject(errm);
                }
                resolve(map);
              });
            }
          });
        } else {
          map.set('NoDatas', {
            symbol: 'NoDatas',
            persent: 'NoDatas',
            timeorder: 'NoDatas',
            rate: 'NoDatas',
          });
          mysql.connection.end((errm) => {
            if (errm) {
              console.error(errm);
              reject(errm);
            }
            resolve(map);
          });
        }
      });
    });
  }

  reducePercents(type) {
    let sql = '';

    if (type === 'total') {
      sql = `SELECT ( (ratebuy - ratesell) / ratebuy * (-100) ) as per FROM public.${this.table} WHERE (typeordbuy = $1 AND typeordsell = $2 AND (timeutcsell >= now() - interval '43802 hours') ) ORDER BY timeutcsell DESC`;
    }

    if (type === 'last24hours') {
      sql = `SELECT ( (ratebuy - ratesell) / ratebuy * (-100) ) as per FROM public.${this.table} WHERE (typeordbuy = $1 AND typeordsell = $2 AND (timeutcsell >= now() - interval '26 hours') ) ORDER BY timeutcsell DESC`;
    }

    return new Promise((resolve, reject) => {
      this.postgre.query(sql, ['BUY', 'SELL'], (err, result) => {
        if (err) {
          console.log(err);
          reject(err);
        }

        const persentsD = [];
        if (result.rowCount >= 1) {
          result.rows.forEach((dataset) => {
            const per = parseFloat(dataset.per);
            const perWithComisionTotal = per - (Math.abs(per * 0.15));
            persentsD.push(perWithComisionTotal);
          });

          let resultPers22k = 0;
          if (persentsD.length !== 0) {
            resultPers22k = persentsD.reduce((sum, current) => sum + current, 0);
          } else resultPers22k = 0;

          resolve(resultPers22k);
        } else {
          resolve(0);
        }
      });
    });
  }

  openOrders(redisClient) {
    return new Promise((resolve, reject) => {
      const sqlLL = `SELECT * FROM public.${this.table} WHERE (typeordbuy = $1 AND typeordsell = $2) ORDER BY timeutcbuy DESC LIMIT 25;`;
      this.postgre.query(sqlLL, ['BUY', ''], (err, result) => {
        if (err) {
          console.log(err);
          reject(err);
        }

        const map = new Map();
        if (result.rowCount !== 0) {
          // result.rows.reverse();
          let sumPers = 0;
          this.sumPers = 0;
          result.rows.forEach(async (dataset) => {
            const buysellnow = await Stats.buyNowValue(redisClient, dataset.market);
            // const part11 = parseFloat(dataset.ratebuy) - parseFloat(buysellnow);
            const rbuy = parseFloat(dataset.ratebuy);
            const rnow = parseFloat(buysellnow);
            let perBuy = (rbuy - rnow) / rbuy * 100;
            perBuy *= -1;
            perBuy = parseFloat(perBuy) - (Math.abs(perBuy * 0.075));
            sumPers += perBuy;

            map.set(dataset.uuidord, {
              symbol: dataset.market,
              persent: perBuy.toFixed(2),
              timeorder: dataset.timeutcbuy,
              rate: dataset.ratebuy,
            });

            if (result.rowCount === map.size) {
              this.sumPers = sumPers;
              resolve(map);
            }
          });
        } else {
          map.set('NoDatas', {
            symbol: 'NoDatas',
            persent: 'NoDatas',
            timeorder: 'NoDatas',
            rate: 'NoDatas',
          });
          resolve(map);
        }
      });
    });
  }

  closeOrders() {
    return new Promise((resolve, reject) => {
      const sqlLL = `SELECT * FROM public.${this.table} WHERE (typeordbuy = $1 AND typeordsell = $2) ORDER BY timeutcsell DESC LIMIT 25;`;
      this.postgre.query(sqlLL, ['BUY', 'SELL'], (err, result) => {
        if (err) {
          console.log(err);
          reject(err);
        }

        if (result.rowCount !== 0) {
          result.rows.reverse();
          const map = new Map();
          result.rows.forEach((dataset) => {
            const rbuy = parseFloat(dataset.ratebuy);
            const rsell = parseFloat(dataset.ratesell);
            let per = (rbuy - rsell) / rbuy * 100;
            per *= (-1);
            per = parseFloat(per) - (Math.abs(per * 0.15));

            map.set(dataset.uuidord, {
              symbol: dataset.market,
              persent: per.toFixed(2),
              timeorder: dataset.timeutcsell,
              rate: dataset.ratesell,
            });

            if (result.rowCount === map.size) {
              resolve(map);
            }
          });
        }
      });
    });
  }

  stop(callback) {
    this.postgre.end();
    return callback(null, null);
  }

  static buyNowValue(redis, symbol) {
    return new Promise((resolve, reject) => {
      redis.pipeline([
        ['get', `${symbol}:bids`],
      ]).exec((err, results) => {
        if (err) {
          console.log(symbol, err);
          reject(err);
        }

        const datasBids = [];
        if (results[0][1] !== null) {
          const bids = JSON.parse(results[0][1]);

          Object.keys(bids).forEach((key) => {
            const obj = {};
            obj.Quantity = bids[key];
            obj.Rate = key;
            datasBids.push(obj);
          });

          resolve(datasBids[0].Rate);
        } else {
          // console.log(symbol, `datasBids[0].Rate is ${results[0][1]}`);
          resolve('0.0');
        }
      });
    });
  }
}

module.exports = Stats;