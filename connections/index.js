const Dbredissubs = require('./redis');

const redissubs = new Dbredissubs();

const Dbredispubs = require('./redis');

const redispubs = new Dbredispubs();

const Dbredisclients = require('./redis');

const redissubslients = new Dbredisclients();

const Dbpostgre = require('./postgre');

const postgre = new Dbpostgre();

const ClientBinance = require('./binance');

const binance = new ClientBinance();

postgre.connect();

class Connection {
  constructor() {
    this.postgre = 5432;

    this.redisclients = redissubslients;
    this.redissubs = redissubs;
    this.redispubs = redispubs;
    this.postgre = postgre;
    this.binance = binance;

    return this;
  }

  connectToPostgre() {
    const postgreOne = new Dbpostgre({
      port: this.postgre,
      host: process.env.POSTGRE_HOST,
      user: process.env.POSTGRE_USER,
      password: process.env.POSTGRE_PASSWORD,
      database: process.env.POSTGRE_DATABASE,
    });
    postgreOne.connect();
    return postgreOne;
  }
}

module.exports = Connection;