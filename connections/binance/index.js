const Binance = require('node-binance-api');
const settings = require('./settings-binance');

class ClientBinance {
  constructor() {
    this.connections = ClientBinance.connectToDb();

    return this.connections;
  }

  static connectToDb() {
    const arr = {};
    let testmode = '';
    if (process.env.ROBOT_MODE === 'dev') {
      testmode = settings.robot.testS;
    }

    if (process.env.ROBOT_MODE === 'prod') {
      testmode = settings.robot.testR;
    }

    arr.robot = new Binance().options({
      APIKEY: process.env.BINR_APIKEY,
      APISECRET: process.env.BINR_APISECRET,
      reconnect: settings.robot.reconnect,
      recvWindow: settings.robot.recvWindow,
      useServerTime: settings.robot.useServerTime,
      verbose: settings.robot.verbose,
      test: testmode,
    });

    arr.history = new Binance().options({
      APIKEY: process.env.BINM_APIKEY,
      APISECRET: process.env.BINM_APISECRET,
      reconnect: settings.market.reconnect,
      recvWindow: settings.market.recvWindow,
      useServerTime: settings.market.useServerTime,
      verbose: settings.market.verbose,
      test: testmode,
    });

    return arr;
  }
}

module.exports = ClientBinance;
